package utils

import (
	"math/rand"
	"time"
)

var runes = []rune("1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM")
const size int = 8

func RandomURL() string {
	rand.Seed(time.Now().UnixNano())
	str := make([]rune, size)

	for i := range str {
		str[i] = runes[rand.Intn(len(runes))]
	}

	return string(str)
}