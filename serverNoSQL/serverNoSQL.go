package serverNoSQL

import (
	"strings"
	"boo.bl/utils"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

var links = make([]string, 0)
var shortLinks = make([]string, 0)
var count int = 0

func redirect(c* fiber.Ctx) error {
	shortLink := c.Params("shortLink")
	err := true
	var link string
	for i := 0; i < count; i++{
		if shortLinks[i] == shortLink {
			link = links[i]
			err = false
			break
		}
	}
	if err {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": "[ERROR] Get DB entry: this short link is not in the database",
		})
	}

	return c.Redirect(link, fiber.StatusTemporaryRedirect)
}

func getLink(c *fiber.Ctx) error {
	shortLink := c.Params("shortLink")

	err := true
	var link string
	for i := 0; i < count; i++{
		if shortLinks[i] == shortLink {
			link = links[i]
			err = false
			break
		}
	}
	if err {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": "[ERROR] Get DB entry: this short link is not in the database",
		})
	}
	
	result := map[string]string{"redirect": link}
	return c.Status(fiber.StatusOK).JSON(result)
}

func createEntry(c *fiber.Ctx) error {
	c.Accepts("application/json")

	links = append(links, "")
	shortLinks = append(shortLinks, "")
	var body map[string]string
	err := c.BodyParser(&body)
	if !(strings.Contains(body["redirect"], "https://") || strings.Contains(body["redirect"], "http://")) {
		body["redirect"] = "https://" + body["redirect"]
	}
	links[count] = body["redirect"]
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": "[ERORR] JSON Parsing:" + err.Error(),
		})
	}

	
	shortLinks[count] = utils.RandomURL()
	
	result := map[string]string{"shortLink": c.BaseURL() + "/" + shortLinks[count]}
	count++
	return c.Status(fiber.StatusOK).JSON(result)
}

func SetupAndListen() {
	router := fiber.New()

	router.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))
	router.Get("/:shortLink", getLink)
	router.Post("/", createEntry)
	router.Get("/redirect/:shortLink", redirect)

	router.Listen(":3000")
}