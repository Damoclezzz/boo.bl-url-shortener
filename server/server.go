package server

import (
	"strings"
	"boo.bl/model"
	"boo.bl/utils"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func redirect(c* fiber.Ctx) error {
	shortUrl := c.Params("shortLink")
	entry, err := model.FindByShortUrl(shortUrl)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": "[ERROR] Could not find short link in DB:" + err.Error(),
		})
	}
	
	return c.Redirect(entry.Redirect, fiber.StatusTemporaryRedirect)
}

func getLink(c *fiber.Ctx) error {
	shortLink := c.Params("shortLink")

	entry, err := model.GetEntry(shortLink)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": "[ERROR] Get DB entry:" + err.Error(),
		})
	}
	
	result := map[string]string{"redirect": entry.Redirect}
	return c.Status(fiber.StatusOK).JSON(result)
}

func createEntry(c *fiber.Ctx) error {
	c.Accepts("application/json")

	var entry model.Boobl
	err := c.BodyParser(&entry)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": "[ERORR] JSON Parsing:" + err.Error(),
		})
	}

	if !(strings.Contains(entry.Redirect, "https://") || strings.Contains(entry.Redirect, "http://")) {
		entry.Redirect = "https://" + entry.Redirect
	}
	entry.Short = utils.RandomURL()

	err = model.CreateEntry(entry)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": "[ERROR] Create model:" + err.Error(),
		})
	}
	
	result := map[string]string{"shortLink": c.BaseURL() + "/" + entry.Short}
	return c.Status(fiber.StatusOK).JSON(result)
}

func SetupAndListen() {
	router := fiber.New()

	router.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))
	router.Get("/:shortLink", getLink)
	router.Post("/", createEntry)
	router.Get("/redirect/:shortLink", redirect)

	router.Listen(":3000")
}