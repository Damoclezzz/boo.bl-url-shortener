package main

import (
	"flag"
	"fmt"
	"boo.bl/model"
	"boo.bl/server"
	"boo.bl/serverNoSQL"
)

func main() {
	dbFlag := flag.Bool("d", false, "Should run with database connection?")
	flag.Parse()
	if *dbFlag {
		model.Setup()
		server.SetupAndListen()
	} else {
		fmt.Println("NoSQL mode")
		serverNoSQL.SetupAndListen()
	}
}