package model

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)
var db *gorm.DB

type Boobl struct {
	ID       uint64 `json:"id" gorm:"primaryKey"`
	Redirect string `json:"redirect" gorm:"not null"`
	Short    string `json:"shortLink" gorm:"unique;not null"`
}

func Setup() {
	dsn := "host=127.0.0.1 user=admin password=admin dbname=urlShortener port=5432 sslmode=disable"
	var err error
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	err = db.AutoMigrate(&Boobl{})
	if err != nil {
		fmt.Println(err)
	}
}