package model

func GetEntry(shortLink string) (Boobl, error) {
	var entry Boobl

	tx := db.Where("short = ?", shortLink).First(&entry)

	if tx.Error != nil {
		return Boobl{}, tx.Error
	}

	return entry, nil
}

func CreateEntry(entry Boobl) error {
	tx := db.Create(&entry)
	return tx.Error
}

func FindByShortUrl(url string) (Boobl, error) {
	var entry Boobl
	tx := db.Where("short = ?", url).First(&entry)
	return entry, tx.Error
}